
/**
 * @author wdd
 * @date September 11th 2020
 * Class Attachment
 * An abstraction of a file which may be "attached" to an issue, project, or comment!
 * This class will likely be extended into classes like PictureAttachment, DocumentAttachment, MovieAttachment, AudioAttachment, RunnableAttachment
 * 
 */
public abstract class Attachment implements Saveable {
	public int id;
	public String Name;
	public String Location; // such as https://www.williamdoyle.ca/arthropod/attachment_storage/projectDiagram2.png
	
	public String toString() {
		return this.Name + " @ " + this.Location;
	}

	protected String localCopy = null;
	/**mode
	 * September 11th 2020
	 * William Doyle
	 * DownloadLocalCopy();
	 */
	public void DownloadLocalCopy() {
		// download the file to the local pc so that it can be shown
		this.localCopy = "local address of file here";
	}
	
	/**
	 * @author wdd
	 * this method does whatever it needs to do to display the file. If this is an audio file play it! Image? Show it! 
	 * @tip use protected string --> localCopy
	 */
	public abstract void show();
	
	@Override
	public int save() {
		/* save: as in save this data to the database 
		 * this is not about "saving" actual files. We only need to save description of the file.
		 * One of these descriptive attributes is the location of the file on a web server.
		 * */
		return Connector.SaveAttachment(this);
	}
	

}
