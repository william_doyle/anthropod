import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * 
 */

/**
 * @author wdd
 *
 */
public class NewThingMenu extends JPopupMenu {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6430934463601157363L;

	
	
	public NewThingMenu () {
		super();
		
		// Define the items
		JMenuItem items [] = {
				new JMenuItem("New Project"),
				new JMenuItem("New Issue")
		};
		
		// All all items to self
		for (JMenuItem item : items) {
			item.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// get text from the menu item that was selected
					String itemText = item.getText();
					/* not great to use these strings for logic. They should only be used to convary information. Bad code but I will rework this later.. William Doyle */
					if (item.getText().toLowerCase() == "new project") {
						
					}
					else if (item.getText().toLowerCase() == "new issue") {
						
					}
				}
				
			});
			this.add(item);
		}
	}
}
