import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Class NewProjectJPanel
 * @author wdd
 * Designed to isolate this functionality (new project creation) to a reusable and
 * contained class
 * @extends JPanel
 * @date September 13th 2020
 *
 */

public class NewProjectJPanel extends JPanel {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = -901371968041560636L;

	public NewProjectJPanel() {
		super();
		setLayout(new GridLayout(3,2));
		setBorder(
	            BorderFactory.createTitledBorder("Start A New Project"));
	
		JLabel projectNameLbl = new JLabel("Project Name: ");
		JLabel projectDescLbl = new JLabel("Description: ");
		
		JTextField projectName = new JTextField(15);
		JTextField projectDesc = new JTextField(15);
		
		JButton AddProjectBTN = new JButton("New Project");
		
		AddProjectBTN.addActionListener(new ActionListener() {

			/**
			 * @author William Doyle
			 * User pressed a button and wants to create a new project 
			 * based on the values they already entered into the text boxes
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				// Create project object from text fields
				Project project = new Project(projectName.getText(), projectDesc.getText());
				
				// save project on database
				// use result to determine success state
				String message = "";
				if (project.save() > 0) {
					message = "Success! Created New Project!";
				}
				else {
					message = "Failed to create new project!";
				}
				// Notify user of success state via pop up
				JOptionPane.showMessageDialog(NewProjectJPanel.this, message);
			}
			
		});
		// Add components in order of apperience on panel
		add(projectNameLbl);
		add(projectName);
		add(projectDescLbl);
		add(projectDesc);
		add(AddProjectBTN);
	}
}
