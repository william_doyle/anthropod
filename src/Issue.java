/**
 * 
 */


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 * @author wdd
 *
 */
public class Issue implements Saveable {
	public int id;
	public int fk_project_id;
	public String description;
	public String title;
	public boolean active;
	public Vector<Comment> comments;
	public Vector<String> attatchments; // address of any attatched files
	
	Issue (String __title, String ___desc, int fk) {
		this.OpenIssue();// start all issues as open
		this.description = ___desc;
		this.fk_project_id = fk;
		this.comments = new Vector<Comment>();
		
		this.attatchments = new Vector<String>();
		this.title = __title;
	}
	
	
	/**
	 * William Doyle
	 * GetAllOpenIssues() 
	 * gets all open issues
	 */
	public static Vector <Issue> GetOpenIssues() {
		return Connector.GetAllOpenIssues();
	}
	
	/* 
	 * Returns All Closed issues
	 */
	public static Vector <Issue> GetClosedIssues() {
		Vector<Issue> closedIss = new Vector<Issue>();
	
		return closedIss;
	}
	
	/*
	public void AddAttachment(Attachment attachment) {
		
	}
	
	public void AddComment(String sCommentBody) {
		
	}
	*/
	
	public void OpenIssue () {
		this.active = true;
	}
	
	public void CloseIssue () {
		// close the issue
		// make sure state of issue is persisted over graphical and informational 
		this.active = false;
		// update db to reflect this 
		
	}
	
	
	/*
	 * Generate issue from data in the data base.. used for retrieving issues from database
	 * 
	 */
	public static Vector<Issue> GetAllIssuesOfProject(int id) {
		/* let the connector class do the work */
		return Connector.GetAllIssuesOfProject(id);
	}

	public String toString() {
		return "Bug " + this.title;//("Issue {Project id: " + this.fk_project_id + ", Issue id: " + this.id +  ", Name: " + this.title + ", Description: " + this.description + "}").substring(0,staticHelp.char_limit);
	}
	
	/* save the issue to the database */
	@Override
	public int save() {
		// No actual db logic should be in this class... let Connector do the hard work
		return Connector.SaveIssue(this);	
	}
	
}
