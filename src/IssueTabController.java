import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * 
 */

/**
 * @author wdd
 *	switching to GridLayout from GridBagLayout September 5th 2020
 */
public class IssueTabController extends JPanel {
	
	public static Vector<Project> projects = new Vector<Project>();
	public JButton AddIssueBTN;
	public JComboBox<Project> projectCBOX;
	
	
	/*
	 * refresh vector of projects for the issue page
	 * refresh jcombobox
	 * 
	 */
	public  void loadProjectsFromDb_for_issue_page() {
			projects = Connector.GetAllProjects();
			projectCBOX.removeAllItems();
			for (Project p : projects) {
				projectCBOX.addItem(p);
			}
	}
	
	
	
	public IssueTabController() {
		this.setLayout(new FlowLayout());
		
		JPanel jpNewIssue = new JPanel();
		jpNewIssue.setLayout(new GridLayout(0,2));
		JLabel bugNameLbl = new JLabel("Bug Name");
		JTextField bugNameField = new JTextField(7);
		jpNewIssue.add(bugNameLbl);
		jpNewIssue.add(bugNameField);
		JLabel bugDescLbl = new JLabel("Bug Description");
		JTextArea bugDescField = new JTextArea(7, 15);
		
		jpNewIssue.add(bugDescLbl);
		jpNewIssue.add(bugDescField);
		
		JLabel projectDropDownLbl = new JLabel("Select A Project");
	//	IssueTabController.loadProjectsFromDb_for_issue_page(); // refreshstat
		this.projectCBOX = new JComboBox<Project>(projects);
		
		jpNewIssue.setBorder(
	            BorderFactory.createTitledBorder("Report A New Bug"));
		
		{
			AddIssueBTN = new JButton("Submit Bug");
			AddIssueBTN.addActionListener(new ActionListener() {

				/*
				 * William Doyle
				 * Do This when AddIssueBTN is pressed
				 * 
				 */
				@Override
				public void actionPerformed(ActionEvent e) {
					Issue bug = new Issue (
							bugNameField.getText(), 
							bugDescField.getText(), 
							((Project)projectCBOX.getSelectedItem()).databaseId
							);
					
					// notify user of success
					String message = "";
					if (bug.save() > 0) {
						message = "Success! Issue Created!";
					}
					else {
						message = "Failed to create issue!";
					}
					JOptionPane.showMessageDialog(IssueTabController.this,  message);
				}
				
			});
			jpNewIssue.add(projectDropDownLbl);
			jpNewIssue.add(projectCBOX);
			jpNewIssue.add(AddIssueBTN);
			add(jpNewIssue);
		}
		setVisible(true);
	}
}
