import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;

public class MainWindow extends JFrame
{
	private static final long serialVersionUID = 1L;

	JFrame _mainWindow;
	
	Dimension _screenSize = getToolkit().getScreenSize();
	
	public static String PATH_TO_ICON_NCONNECTED = "not_connected.png";
	public static String PATH_TO_ICON_CONNECTED = "connected.png";
	
	public static ImageIcon img = new ImageIcon(PATH_TO_ICON_NCONNECTED);
	
	// Make a public reference for all of our tabs... That way they can be accessed from other parts of the GUI
	public IssueTabController issueTabObj;
	public LoginTabController loginTabObj;
	public ProjectTabController projectTabObj;
	public SettingsTabController settingsTabObj;
	
	public Image icon;

	public JLabel limg;
	
	public void make_icon_nconnected() {
	//	img = new ImageIcon(PATH_TO_ICON_NCONNECTED);
	//	setIconImage(img.getImage());
		setIconImage(Toolkit.getDefaultToolkit().getImage(PATH_TO_ICON_NCONNECTED));
	//	 
	//	repaint();
	}
	
	public void make_icon_connected() {
	//	img = new ImageIcon(PATH_TO_ICON_CONNECTED);978-1619608634
		//setIconImage(img.getImage());
		setIconImage(Toolkit.getDefaultToolkit().getImage(PATH_TO_ICON_CONNECTED));
		//repaint();
		
	}
	

	
	public MainWindow() 
	{
		// load language pack 
		staticHelp.tvr = new TextValueResourses("language.json");
		
		
		
		_mainWindow = new JFrame();
		_mainWindow.setLayout(new BorderLayout());
		_mainWindow.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(0, 0, _screenSize.width-100, _screenSize.height-100);
		
		// maintain a way of accessing the tab controllers
		this.issueTabObj = new IssueTabController();
		this.loginTabObj = new LoginTabController();
		this.projectTabObj = new ProjectTabController();
		this.settingsTabObj = new SettingsTabController();
		
		
		tabbedPane.add("Login", this.loginTabObj);
	//	tabbedPane.add("Current", new CurrentTabController());
	//	tabbedPane.add("Closed", new ClosedTabController());
		tabbedPane.add("Issue View", this.issueTabObj);
		tabbedPane.add("Project View", this.projectTabObj);
		tabbedPane.add("Settings", this.settingsTabObj);
		
		limg = new JLabel(new ImageIcon(PATH_TO_ICON_CONNECTED));
		_mainWindow.add(limg);
		
		icon = Toolkit.getDefaultToolkit().getImage(PATH_TO_ICON_CONNECTED); 
		_mainWindow.setIconImage(icon);
		_mainWindow.setTitle("Anthropod");
		//make_icon_nconnected();
		
		JPanel wrap = new JPanel();
		wrap.setLayout(new BorderLayout());
			JToolBar tb = new JToolBar();
			
				JPanel tbp = new JPanel();
				
					// create and add tool bar items to this panel "tbp" or "tool bar panel"
					JButton fileBtn = new JButton("File");
					JButton editBtn = new JButton("Edit");
					JButton createNewBtn = new JButton("New");
					
					createNewBtn.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							//JPanel mu = new JPanel();
							NewThingMenu ntm = new NewThingMenu();
							_mainWindow.add(ntm);
							//ntm.show();
							
						}
						
					});
					
					tbp.add(fileBtn);
					tbp.add(editBtn);
					tbp.add(createNewBtn);
				
				tb.add(tbp);
			
			
			wrap.add(tb, BorderLayout.NORTH);
		wrap.setSize(this.getWidth(), this.getHeight());
		wrap.add(tabbedPane);
		_mainWindow.add(wrap, BorderLayout.NORTH);
		_mainWindow.setSize(this.getWidth(), this.getHeight());
		//_mainWindow.add(tabbedPane);
		//_mainWindow.setSize(_screenSize.width-1500, _screenSize.height-500);
		_mainWindow.setVisible(true);
		_mainWindow.setExtendedState(_mainWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		pack();
	}
}
