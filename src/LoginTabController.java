import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class LoginTabController extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8405678024857541050L;
	
	public JButton onlyButton; // Taking a one button approch.. state 1: login, State Two: logout 
	
	final protected String LOGIN_BTN_TXT = "Login";
	final protected String LOGOUT_BTN_TXT = "Logout";
	
	public static short state = 0;
	
	public JTextField unameField;
	public JTextField pwordField;
	
	public static String GetLastUserName() {
		return "Last username here";
	}
	
	public static User clientSideUserObject;		// our local java copy of a user object
	
	
	public LoginTabController() {
		setLayout(new GridBagLayout());
		this.setBorder(
	            BorderFactory.createTitledBorder("Arthropod > Login, Logout, And Register"));
		
		
		GridBagConstraints c = new GridBagConstraints();
	
		
		this.onlyButton = new JButton(LOGIN_BTN_TXT);
		
		final String sLabelUsernameField = staticHelp.tvr.ValueFromId("usrname");//"Username: ";
		final String sLabelPasswordField = staticHelp.tvr.ValueFromId("pword");
		
		this.unameField = new JTextField(7);
		this.pwordField = new JTextField(7);
		
		
		JLabel jLabelUsernameField = new JLabel(sLabelUsernameField);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(jLabelUsernameField, c);	
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		this.add(this.unameField, c);
				
		JLabel jLabelPasswordField = new JLabel(sLabelPasswordField);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		add(jLabelPasswordField, c);	
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		this.add(this.pwordField, c);
		
		this.onlyButton.setBackground(Color.RED); // start red for not connected	
		this.onlyButton.addActionListener(new ActionListener() {

		
			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * 		THE LOGIN BUTTON WAS PRESSED
				 * 
				 */
				switch (state) {
				case 0:
					// Login mode
					// 1. Get text from text feilds
					String sUserNameFromTextBox = "wdd";//"WilliamAnthropod";//unameField.getText();	// Later On Remove uselssvariables
					String sPasswordFromTextBox = "password";//pwordField.getText();
					// 2. Send the values to login system
					clientSideUserObject = new User(sUserNameFromTextBox, sPasswordFromTextBox);
					Connector.setUser(clientSideUserObject);
					try {
						Connector.ConnectToDatabase();
					} catch (SQLException e1) {
						e1.printStackTrace();
						System.out.println("[BAD] Failed To Connect to Database");
					}
					
					// 3. test login system for success
					boolean loginSuccess = Connector.isConnected();
					
					// 4. only upon confirmation of success:
					if (loginSuccess == true) {
					//		--> Hide the Text boxs
						unameField.setFocusable(false);
						jLabelUsernameField.setForeground(Color.GRAY);
						
						pwordField.setFocusable(false);
						pwordField.setText("********");
						jLabelPasswordField.setForeground(Color.GRAY);
						//		--> Change button text to LOGOUT_BTN_TXT
						onlyButton.setText(LOGOUT_BTN_TXT);
						onlyButton.setBackground(Color.GREEN);
						//		--> set state to 1
						LoginTabController.state = 1;
						
						staticHelp.mwinRef._mainWindow.setTitle("Anthropod: Connected");
						System.out.println("[NORMAL] Login Success");
						
						
						///	Refresh other GUI pages that need to be updated after login
						
						// Access issue tab via static reference to mainwindow witch itself has a reference to the Issue Tab
						// This feels slopy but i'll point out that this is entirly contained to the GUI layer of the application
						staticHelp.mwinRef.issueTabObj.loadProjectsFromDb_for_issue_page();
						
					}
					else {
					// 5. Only upon confirmation of failure to login:
					//		--> Notify user of failure

						System.out.println("[BAD] Could Not Log User In");
					}
					break;
				case 1:
					// 1. Log user out
					// 		a. Ask user to save work
					Connector.DisconnectFromDatabase();
					boolean logoutSuccess = true; 
					if (logoutSuccess == true) {
						// 2. Restore page to state == 0
						unameField.setFocusable(true);
						unameField.setForeground(Color.BLACK);
						jLabelUsernameField.setForeground(Color.BLACK);
						unameField.setText(LoginTabController.GetLastUserName());
						
						pwordField.setFocusable(true);
						pwordField.setText("");
						jLabelPasswordField.setForeground(Color.BLACK);					
						pwordField.setForeground(Color.BLACK);							//	Set text Color back to black
						
						onlyButton.setText(LOGIN_BTN_TXT);								//	restore button text to login phrase
						onlyButton.setBackground(Color.RED);
						LoginTabController.state = 0;									// 	restore internal state to 0 
						
						System.out.println("[NORMAL] Logout Success");
						
						staticHelp.mwinRef.make_icon_nconnected();
						staticHelp.mwinRef._mainWindow.setTitle("Anthropod: Disconnected");
					}
					else {
						/* Notify user of failure */
						System.out.println("[BAD] Could Not Log User Out");
					}
					
					break;
				default:

					System.out.println("[BAD] Could Not Log User Out");
					break;
						
				}
			}
		});
			
	//	this.add(entryFieldsAndLabels);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = GridBagConstraints.REMAINDER; // makes the login button stretch
		this.add(this.onlyButton, c);
		
		/* Register button Start */
		{
			/* Create button */
			JButton regstrbtn = new JButton("New User");
			
			/* Configure Button apperience and behaviour */
			/* Apperience */
			regstrbtn.setBackground(Color.LIGHT_GRAY);
			/* Behaviour */
			regstrbtn.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("[Normal] User Pressed Button To Create New Profile");
					JOptionPane.showMessageDialog(LoginTabController.this, "Please see your Administrator to create a new user for this Anthropod system");
				}
				
			});
			
			/* set up constraints */
			c.gridx = 0;
			c.gridy = 3;
			c.gridwidth = GridBagConstraints.REMAINDER; // makes the login button stretch
			
			/* add the button with constraints */
			this.add(regstrbtn, c);
		}
		/* Register Button end */
		
		
		setVisible(true);
	}
}
