import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Vector;

import org.json.simple.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * @author william
 *
 */
public class TextValueResourses {
	

	public Vector<TextIdPair> text_id_pair = new Vector<TextIdPair>();
	
	public String ValueFromId (String _id) {
		for (TextIdPair tip : this.text_id_pair ) {
			if (tip.Id == _id) {
				return tip.Text;
			}
		}
		return "";
	}
	
	public TextValueResourses (String filename) {
		// Read data file in to this->textValues
		//JSONArray arr = new JSONArray(filename);
		
		String content;
		try {
			content = new String(Files.readAllBytes(Paths.get(filename)));
		//	System.out.println(content);
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject)parser.parse(content);
			
			JSONArray jarr = (JSONArray)obj.get("english");
			for (int i = 0; i < jarr.size(); i++) {
				JSONObject jsonTextIdPair = (JSONObject)jarr.get(i);
				this.text_id_pair.add(new TextIdPair((String)jsonTextIdPair.get("id"), (String)jsonTextIdPair.get("value")));
			}
			
		} catch (IOException e) {
			// problem with file
			System.out.println("[Bad] Problem With File");
			e.printStackTrace();
		} catch (ParseException e) {
			// problem with contents of file
			System.out.println("[Bad] Problem With File Contents");
			e.printStackTrace();
		}
		
		
			
		
		
	}
}
