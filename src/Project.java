
import java.sql.SQLException;
import java.util.Vector;

/**
 * @author wdd
 *	Abstraction of a project. This application track projects and issues as they develop.
 */
public class Project implements Saveable{
	
	public Vector<Issue> issues; 	// issues beloging to this project
	public int databaseId; 			// the database id of this project
	public String name;				// The name of this project
	public String sDesc = "";		// the description of this object
	
	public Project () {
		
	}
	
	/**
	 * @author wdd
	 * Args 2
	 * 	1. string -- name of project
	 * 	2. string -- description of the project 
	 */
	public Project (String s, String desc) {
		this.name = s;
		this.sDesc = desc;
		this.issues = new Vector<Issue>();
	}
	
	/**
	 * @author wdd
	 */
	public String toString() {
		return "Project " + this.name;
	}
	
	/**
	 * @author wdd
	 * Save this project to the database
	 * this is done using the static method SaveProject()
	 * found in the Connector Class
	 */
	@Override
	public int save() {
		// let connector do hard work
		return Connector.SaveProject(this);
	}
	
	
	
/* S T A T I C   F U N C T I O N S */
	
	/**
	 * @author wdd
	 * @return
	 */
	public static Vector<Project> GetAllProjectsFromDb(){
		return Connector.GetAllProjects();
	}
	
	/**
	 * @author wdd
	 * @param sProjectName
	 * @return
	 */
	public static Project GetProjectFromDbByName (String sProjectName) {
		Project project = new Project(sProjectName, "");
		project.sDesc = "Description as found in database";
		project.databaseId = 1; // Temp
		// get issues from the junction database
		
		return project;
	}
	
	
}
