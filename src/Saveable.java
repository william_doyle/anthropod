
/**
 * @author william
 * Single Abstract Method For Saving info to database
 */
public interface Saveable {
	/**
	 * Typically this function should do nothing
	 * except call a static function belonging to
	 * the Connector Class. This is done to improve 
	 * organization of code and to allow Antropod to 
	 * be adapted to use another kind of server simply
	 * by rewriting the connector class!
	 * 
	 */
	public int save();
}
