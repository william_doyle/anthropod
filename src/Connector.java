/**
 * 
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
//import com.sun.;

/**
 * @author william
 * The only way we should be connecting to the database or using the database in any way
 */
public class Connector {
	
	public static boolean connected = false;
	protected static User user;
	public static Connection connection;
	
	/**
	 * @author wdd
	 * @date pre September 13th 2020
	 * Set static value of user
	 * @param passedUser
	 */
	public static void setUser (User passedUser) {
		Connector.user = passedUser;
	}
	
	/**
	 * Returns connected if connection is valid
	 * else returns false
	 * @author wdd
	 * isConnected()
	 * Use this for healthy validation of the database connection
	 * 
	 */
	public static boolean isConnected() {
		// if connected return true
		try {
			if (connection.isValid(3)) {
				return true;
			}
			else {
				return false;
			}
			// if not connected see if we are trying to connect
			// if we are wait a moment then return isConnected()
			// else return false
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} // place holder
	}
	
	/**
	 * Short cut
	 * @throws SQLException
	 */
	public static void ConnectToDatabase() throws SQLException{
		ConnectToDatabase(	3306, 							
							"arthropod", 
							"jdbc:mysql://localhost:3306/arthropod?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
							);
	}
	
	/**
	 * @author wdd
	 * ConnectToDatabase()
	 * Connects to the database
	 * Currently only uses my login... just for quick developing
	 * 
	 */
	public static void ConnectToDatabase(int _host_number, String db_name, String _url) throws SQLException{
		// Connect to the database based on Connector->user values
		int host_number = _host_number;
		String database_name = db_name;//"arthropodDB";
		String url = _url;// "jdbc:mysql://localhost:3306/arthropod?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		//"jdbc:mysql://localhost:3306/arthropodDB?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	
		try {
			Connector.connection = DriverManager.getConnection(
					url,
					user.uname,
					user.pword // wdd
					);
			System.out.println("Connected to MySQL");
		} catch (SQLException e) {
			System.out.println("[Bad] Failed to Connected to MySQL");
			e.printStackTrace();
			throw e; // pass it up
		} 
	}
	
	/**
	 * @author wdd
	 * @
	 * @return a vector of projects from the data base
	 * @warning makes call to database. THIS METHOD IS SLOW
	 */
	public static Vector<Project> GetAllProjects() {
		Vector<Project> projects = new Vector<Project>();
		
		String sSql = "select * from Projects";
		/* Populate vector with data from database */
		try {
			ResultSet results = Connector.run_sql(sSql);
			
			while (results.next()) {
				final int num_cols = 3;
				Project p = new Project(results.getString(2), results.getString(3));
				p.databaseId = results.getInt(1);
				projects.add(p);
				//System.out.println("[STUB]	PROJECT:" + p.toString());
			}	
			return projects;
			
		} catch (NullPointerException npe) {
			System.out.println("[Bad] Null Pointer Exception... Moving on.");
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("[Bad] Could not find all projects");
			e.printStackTrace();
		}
		
		return projects;
	}
	
	/**
	 * @Date September 9th 2020
	 * @author wdd
	 * @method name GetAllIssuesOfProject()
	 * gets all the issues from the database with a forign key (indicating related project) matching the provided int 
	 * @param id
	 * @return
	 */
	public static Vector<Issue> GetAllIssuesOfProject(int id) {
		Vector<Issue> issues = new Vector<Issue>();
		
		String s = "select * from Issues where fk_project_id = " + id;
		ResultSet results;
		try {
			results = Connector.run_sql(s);
			while (results.next()) {
				final int num_cols = 4;
				Issue i = new Issue(results.getString(4), results.getString(2), results.getInt(3));
				i.id = results.getInt(1);
				issues.add(i);
			}
		} catch (SQLException e) {
			System.out.println("[Bad] problem finding issues...");
			e.printStackTrace();
		}
		
		return issues;
	}
	
	/**
	 * Date: Spetember 9th 2020
	 * William Doyle
	 * GetAlCommentsByIssueId()
	 * All comments have an issue id forign key. This method gets all comments with the provided int equil to this forign key.
	 * @param issue_id
	 * @return
	 */
	public static Vector<Comment> GetAllCommentsByIssueId(int issue_id){
		Vector<Comment> comments = new Vector<Comment>();
		
		// get all comments from db with provided id
		String sSql = "select * from Comments where fk_issue_id = " + issue_id;
		try {
			ResultSet results = Connector.run_sql(sSql);
			while (results.next()) {
				final int num_cols = 4;
				// put them into vector comments
				Comment c = new Comment (results.getInt(1), results.getInt(2), results.getInt(3)/*issue id*/, results.getString(4));
				System.out.println("[Stub] Comment Contents: " + issue_id );
				comments.add(c);
			}
		} catch (SQLException e) {
			System.out.println("[Bad] Failed To Load Comments for Issue Id: " + issue_id );
			e.printStackTrace();
		}
		
		// return vector comments
		return comments;
	}
	
	/**
	 * William Doyle
	 * GetAllOpenIssues()
	 * Returns all the issues in the db that are open
	 * @return
	 */
	public static Vector<Issue> GetAllOpenIssues(){
		Vector<Issue> issues = new Vector<Issue>();
	
		// get the issues into the vector
		
		
		// terutn yhr vector
		return issues;
	}
	
	/**
	 * 
	 * run_sql_update()
	 * runs s: a string of sql code. Use this when you want to 
	 * modify data... not just view it.
	 * @author wdd
	 * @param s
	 * @return
	 * @throws SQLException
	 */
	public static int run_sql_update(String s) throws SQLException {
		Statement statement = connection.createStatement();
		return statement.executeUpdate(s);
	}

	/**
	 * @author wdd
	 * @description run any sql requiring use of Statement->executeQuery()
	 * @param s
	 * @return ResultSet
	 * @throws SQLException
	 */
	public static ResultSet run_sql(String s) throws SQLException {
			Statement statement;
			ResultSet results;
			try {
				statement = connection.createStatement();
				results = statement.executeQuery(s);
			} catch (SQLException e) {
				System.out.println("[Bad] run_sql() failed. The Query was: " + s);
				throw e;
			}
			return results;
	}

	/**
	 * SaveProject()
	 * William Doyle
	 * September 9th 2020
	 * Code to save project on the database
	 * @param pro
	 * @return
	 */
	public static int SaveProject(Project pro) {
		int count_lines_altered = 0;
		String command = "insert into Projects (name, description) Values (\"" + pro.name + "\",\""+ pro.sDesc + "\");";
		try {
			count_lines_altered = Connector.run_sql_update(command);
			System.out.println("[Good] Project Saved! count_lines_altered is " + count_lines_altered + ".");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("[BAD] Save Project Failed");
			e.printStackTrace();
		}
		return count_lines_altered;
	}
	
	/**
	 * SaveIssue()
	 * William Doyle
	 * September 9th 2020
	 * Save issue on database
	 */
	public static int SaveIssue(Issue iss) {
		int count_lines_altered = 0;
		String command = "insert into Issues (name, description, fk_project_id) Values (\"" + iss.title + "\",\""+ iss.description + "\",\"" + iss.fk_project_id + "\");";
		try {
			count_lines_altered = Connector.run_sql_update(command);
			System.out.println("[Good] Issue Saved! count_lines_altered is " + count_lines_altered + ".");
		} catch (SQLException e) {
			System.out.println("[BAD] Save Issue Failed");
			e.printStackTrace();
		}
		return count_lines_altered;
	}
	
	/**
	 * SaveComment()
	 * William Doyle
	 * September 9th 2020
	 * Save Comment on database
	 */
	public static int SaveComment(Comment com) {
		// save comment to database
		// fix indentation
				int count_lines_altered = 0;
								
				String command = "insert into Comments "
						+ "(fk_user_id, fk_issue_id, content) "
						+ "Values (\"" + com.user_id + "\",\""+ com.issue_id + "\",\""+
								com.value + "\");";
				try {
					count_lines_altered = Connector.run_sql_update(command);
					System.out.println("[Good] Comment Posted! count_lines_altered is " + count_lines_altered + ".");
				} catch (SQLException e) {
					
					// record event to out stream
					System.out.println("[BAD] Comment Post Failed"); // clearly indicates what happened..
					e.printStackTrace();
				}
				return count_lines_altered;
	}
	
	/**
	 * SaveAttachment()
	 * William Doyle
	 * September 9th 2020
	 * Save issue on database
	 */
	public static int SaveAttachment(Attachment a) {
		System.out.println("[Stub] Connector tries to save attachment");
		return 0;
	}
	
	/**
	 * @author wdd
	 * Disconnects the user from the database
	 */
	public static void DisconnectFromDatabase() {
		// Properly disconnect from database
		try {
			connection.close();
			System.out.println("[Normal] Disconnected from MySQL successfully");
		} catch (SQLException e) {
			System.out.println("[Bad] Failed to disconnect from MySQL!");
			e.printStackTrace();
		}
	}
	
}
