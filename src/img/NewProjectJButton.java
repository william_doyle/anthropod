package img;

import javax.swing.JButton;

/**
 * @author wdd
 * This is a JButton that is used to allow the user to create a new
 * Project
 * 
 */

public class NewProjectJButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4081827972129289563L;
	
	
	public NewProjectJButton(String lbl) {
		super(lbl);
	}
}
