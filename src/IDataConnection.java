/**
 * 
 */


/**
 * @author wdd
 *
 */
public interface IDataConnection {
	public int Connect();
	public int Disconnect();
	
	public int AddUser(User __u);
	public int AddProject(Project __p);
	public int AddIssue(Project __p, Issue __i);
	public int AddComment(Comment __c, Issue __i);
}
