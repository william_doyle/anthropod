
public class TextIdPair {
	
	public String Id;
	public String Text;
	
	public TextIdPair(String _id, String _text) {
		this.Id = _id;
		this.Text = _text;
	}

}
