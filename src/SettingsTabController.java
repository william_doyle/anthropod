import java.awt.*;

import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

/**
 * @author wdd
 * used to control GUI settings 
 */
public class SettingsTabController extends JPanel {

	private static final long serialVersionUID = -5472785237951262545L;
	
	public JButton langSwitch;
	public JButton darkModeSwitch;
	
	public SettingsTabController() {
		this.setLayout(new GridLayout(3,2));
		
		/* Language Switch BTN */
		this.langSwitch = new JButton("Switch Language");
		this.langSwitch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// cycle language
				// update system ui with language pack
				System.out.println("[STUB] Switch UI language.");
			}
			
		});
		this.add(this.langSwitch);
		
		/* Dark Mode Switch BTN */
		this.darkModeSwitch = new JButton("Dark Mode");
		this.darkModeSwitch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("[Normal] Dark Mode Cycle");
				//UIManager.setLookAndFeel(new LookAndFeel());
				try {
					UIManager.setLookAndFeel ("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					// TODO Auto-generated catch block
					System.out.println("[Bad] Dark Mode Set Failed");
					e1.printStackTrace();
				}
			}
			
		});
		this.add(this.darkModeSwitch);

		setVisible(true);
	}
}
