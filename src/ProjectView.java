import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author wdd
 *	September 13th 2020
 */
public class ProjectView extends JPanel {
	
	public JTextField name;
	public JTextField description;
	public JCheckBox allow_editing;
	public JButton updateBtn; // when pressed update item in database based on projectModel.projectid and set the database to be WHATEVER is in the project model
	
	public Project projectModel;
	
	/**
	 * September 13th 2020
	 * @author wdd
	 * AllowEditing of project data
	 */
	public void AllowEditing() {
		this.name.setEditable(true);
		this.description.setEditable(true);
		this.updateBtn.setVisible(true);
	}
	
	/**
	 * September 13th 2020
	 * @author wdd
	 * DenyEditing ability of project. Grey out
	 * text and hide a button
	 */
	public void DenyEditing() {
		this.name.setEditable(false);			// grey out
		this.description.setEditable(false);	// grey out
		this.updateBtn.setVisible(false); 		// hide button
	}

	public ProjectView(Project p) {
		super();
		this.projectModel = p;
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder("Project View"));
		
		name = new JTextField(this.projectModel.name);
		description = new JTextField(this.projectModel.sDesc);
		
		allow_editing = new JCheckBox("Edit");
		allow_editing.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
//					System.out.println("[Normal] Allow User to edit Project Model");
					ProjectView.this.AllowEditing();
				} else {
//					System.out.println("other");
					ProjectView.this.DenyEditing();
				}
				
			}
			
		});
		
		updateBtn = new JButton("Push Project Update To Database");
		updateBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("[Normal] User Pressed Button to update project on database");
				
			}
			
		});
		
		this.DenyEditing(); 
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 0;
		add(name, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 1;
		add(description, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 2;
		add(allow_editing, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 3;
		add(updateBtn, c);
		
		
		
	}
	
	public void refresh() {
		name.setText(this.projectModel.name);
		description.setText(this.projectModel.sDesc);
		
		//name.setSize(this.getWidth(), name.getHeight());
	}
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7606710749110233107L;

	
		
	

}
