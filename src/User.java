import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JOptionPane;




/**
 * @author wdd
 * Abstraction of a user
 */
public class User {
	public int id; // user's id
	public String uname; // user name
	public String pword;

	/**
	 * @author wdd
	 * @param uname
	 * @param pword
	 */
	User (String uname, String pword ){
		this.uname = uname;
		this.pword = pword;
	}
	
	/**
	 * @author wdd
	 * @date Spetember 9th 2020
	 * 
	 * DOES:
	 * Generates a comment based on provided string and this user
	 * returns said comment.
	 * 
	 * DOES NOT:
	 * Does not post the comment
	 * 
	 * @param sCommentBody
	 * @return
//	 */
//	public Comment GenerateComment(String sCommentBody) {
//		Comment c = new Comment(sCommentBody, this);
//		return c;
//	}
	
}
