import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * 
 */

/**
 * @author wdd
 *
 */
public class ProjectTabController extends JPanel {
	
	private static final long serialVersionUID = 8771546455264997866L;
	
	public ProjectView projectView;
	
	/* Buttons */
	public JButton AddProjectBTN;							// Add new project
	public JButton comment_btn = new JButton("comment");	// press to start process of posting a comment
	// labels
	public JLabel issueLbl;
	public JLabel commLbl;
	
	// inputs for new Project
	public JTextField projectName;
	public JTextField projectDesc;
	
	public JComboBox<Issue> issuecbx;
	public JList<Comment> commentcbx;
	public DefaultListModel<Comment> dlmodel;
	
	public JTextArea new_comment_jta;
	public JButton commSubitbtn;
	
	/**
	 * @author wdd
	 * show_issues()
	 * Description: 
	 * @param b
	 */
	private void show_issues(boolean b) {
		ProjectTabController.this.issuecbx.setVisible(b);
		ProjectTabController.this.comment_btn.setVisible(b);
		ProjectTabController.this.issueLbl.setVisible(b);
		ProjectTabController.this.commLbl.setVisible(b);
	}
	
	public ProjectTabController() {
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		projectView = new ProjectView(new Project("Name Here", "Description Here"));
		
		JPanel npp = new NewProjectJPanel(); // this class is a jpanel used for adding new projects
		
		// constraints for the new project jpanel
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		add(npp, c);
			
		JPanel wrap = new JPanel(); // wrap selected project area
			//wrap.setLayout(new GridLayout(3,2));
			wrap.setLayout(new GridBagLayout());
			wrap.setBorder(
		            BorderFactory.createTitledBorder("Information By Bug"));
			
			JComboBox<Project> projectCBOX = new JComboBox<Project>(IssueTabController.projects);

			// blank for now
			this.dlmodel = new DefaultListModel<Comment>();
			this.commentcbx = new JList<Comment>(this.dlmodel);
			commentcbx.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
			commentcbx.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			
			issuecbx = new JComboBox<Issue>();
			issuecbx.addActionListener(new ActionListener() {

				/**
				 * Issue Combo Box Item Selected
				 * @author wdd
				 * @date september 11th 2020
				 */
				@Override
				public void actionPerformed(ActionEvent e) {
					// Get All comments for this issue
					Vector<Comment> vComments = Connector.GetAllCommentsByIssueId(((Issue)issuecbx.getSelectedItem()).id);
					ProjectTabController.this.dlmodel.removeAllElements();
					
					for (Comment _c : vComments) {
						ProjectTabController.this.dlmodel.addElement(_c);
					}
					// Show other widgets
					ProjectTabController.this.comment_btn.setVisible(true);
				}
			});
			
			projectCBOX.addActionListener(new ActionListener() {

				@Override
				/**
				 * @author william Doyle
				 * @date September 11th 2020
				 */
				public void actionPerformed(ActionEvent e) {
					issuecbx.removeAllItems(); // erase current contents
					Vector<Issue> vIssues = Issue.GetAllIssuesOfProject(((Project)projectCBOX.getSelectedItem()).databaseId);
					for (Issue i : vIssues) {
						issuecbx.addItem(i);
					}
					// show the other parts of the form
					ProjectTabController.this.show_issues(true);
					
					
					// also
					projectView.projectModel = new Project( ((Project)projectCBOX.getSelectedItem()).name, ((Project)projectCBOX.getSelectedItem()).sDesc);
					projectView.refresh();
				}				
			});
			
			this.comment_btn.addActionListener(new ActionListener() {

				/**
				 * @author William Doyle
				 * @date September 11th 2020
				 * user wants to write a comment
				 */
				@Override
				public void actionPerformed(ActionEvent e) {
					JFrame f = new JFrame("pop");
					JDialog dialog = new JDialog(f, "Say Something About Issue: \"" + ((Issue)issuecbx.getSelectedItem()).title + "\"", true);
					dialog.setLayout(new GridBagLayout());
					
					new_comment_jta = new JTextArea(/*rows*/17,/*coloums*/17);
					commSubitbtn = new JButton("Post Comment");
					
					commSubitbtn.addActionListener(new ActionListener() {

						/**
						 * @author wdd
						 * @date Sepbember 13th 2020
						 * 
						 */
						@Override
						public void actionPerformed(ActionEvent e) {
							System.out.println("[Normal] User wants to post this comment");
							// TODO Auto-generated method stub
							// post comment to db and get rid of this popup
							int issue_id = ((Issue)issuecbx.getSelectedItem()).id; // get current selection of issue from drop down then get its id as an int
							String body = new_comment_jta.getText();
							Comment comm = new Comment(issue_id, body);
							comm.save();
							
							dialog.setVisible(false);
						}
					});
					
					dialog.setPreferredSize(new Dimension(640, 480));
					
					// Constraints for the text area and the button
					GridBagConstraints gbc = new GridBagConstraints();
					gbc.gridx = 0;
					gbc.gridy = 0;
					gbc.gridwidth = GridBagConstraints.REMAINDER;
					dialog.add(new_comment_jta, gbc);
					
					gbc.gridx = 0;
					gbc.gridy = 1;
					gbc.gridwidth = GridBagConstraints.REMAINDER;
					dialog.add(commSubitbtn, gbc);
					
					dialog.pack();
					dialog.setVisible(true);
				}
				
			});
			
			/* Various Constraints */
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 0;
			c.gridwidth = 3;
			wrap.add(new JLabel("Select Project"), c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 1;
			c.gridwidth = 1;
			this.issueLbl = new JLabel("Select Issue");
			wrap.add(this.issueLbl, c);
			
			c.gridx = 1;
			c.gridy = 1;
			c.ipadx = 50;
			c.gridwidth = GridBagConstraints.REMAINDER;
			this.issuecbx.setVisible(false); // set issue selection box invisable for now
			wrap.add(issuecbx, c);
			
			c.gridx = 1;
			c.gridy = 0;
			c.gridwidth = GridBagConstraints.REMAINDER;
			wrap.add(projectCBOX, c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 2;
			c.ipadx = 0; // restore to default
			c.gridwidth = 1;
			this.commLbl =  new JLabel("Select Comment");
			wrap.add(this.commLbl, c);
			
			c.gridx = 1;
			c.gridy = 2;
			c.gridwidth = GridBagConstraints.REMAINDER;
			wrap.add(this.commentcbx, c);
			
			c.gridx = 0;
			c.gridy = 3;
			c.gridwidth = GridBagConstraints.REMAINDER;
			ProjectTabController.this.show_issues(false); // wait until user has actually selected something before showing them their next options
			wrap.add(this.comment_btn, c);
			
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 1;
			add(wrap, c); // add this wrapper to the scene
			
			c.gridx = 0;
			c.gridy = 2;
			c.gridwidth = GridBagConstraints.REMAINDER;
			add(this.projectView, c);
		
		setVisible(true);
	}
}
