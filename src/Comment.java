import java.sql.SQLException;

/**
 * @author wdd
 *	SQL TABLE outline:
 *
 *	id			int
 *	fk_user_id	int
 *	fk_issue_id	int
 *	content		varchar(255)
 */
public class Comment implements Saveable, IComment {
	public int id;
	public int user_id; // the id of the user who wrote this comment
	public String value;
	private static int IDMAKER = 1;
	public int issue_id;
	
	public Comment ( int _iss_id , String body) {
		this.value = body;
		this.issue_id = _iss_id;
		this.user_id = 0;
	}
	
//	
//	/**
//	 * 
//	 * @param s : string ... comment body value
//	 * @param u : User ... user name of cmment poster
//	 * @deprecated
//	 */
//	public Comment (String s, User u) {
//			
//		this.user_id = u.id;
//		this.value = s;
//		this.id = IDMAKER ++;
//		
//	}
	
	public String toString() {
		return "\"" + this.value + "\" -" + this.user_id;
	}
	
	public Comment(int __id, int __usr_id, int __iss_id, String __content) {
		this.id = __id;				// comment id
		this.user_id = __usr_id;	// user (poster) is
		this.issue_id = __iss_id;	// issue id (comments are ONLY posted on issues)
		this.value = __content;		// content... What does the comment SAY?
	}

	/*
	 * William Doyle
	 * method name: save()
	 * purpose: Saves this comment to the database
	 */
	@Override
	public int save() {
		return Connector.SaveComment(this);
	}
}
